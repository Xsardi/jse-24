package ru.t1.tbobkov.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Argument not supproted...");
    }

    public CommandNotSupportedException(@NotNull final String command) {
        super("Error! Command ''" + command + "'' not supported...");
    }


}
