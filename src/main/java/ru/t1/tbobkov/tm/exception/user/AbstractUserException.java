package ru.t1.tbobkov.tm.exception.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.tbobkov.tm.exception.AbstractException;

public abstract class AbstractUserException extends AbstractException {

    public AbstractUserException() {
    }

    public AbstractUserException(@NotNull final String message) {
        super(message);
    }

    public AbstractUserException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    public AbstractUserException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractUserException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
