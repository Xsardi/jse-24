package ru.t1.tbobkov.tm.exception.user;

public class IncorrectLoginDataException extends AbstractUserException {

    public IncorrectLoginDataException() {
        super("Error! Incorrect login or password...");
    }

}
