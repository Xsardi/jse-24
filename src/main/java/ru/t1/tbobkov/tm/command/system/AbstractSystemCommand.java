package ru.t1.tbobkov.tm.command.system;

import org.jetbrains.annotations.Nullable;
import ru.t1.tbobkov.tm.api.service.ICommandService;
import ru.t1.tbobkov.tm.command.AbstractCommand;
import ru.t1.tbobkov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
